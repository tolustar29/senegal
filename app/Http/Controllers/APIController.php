<?php

namespace App\Http\Controllers;

use Barryvdh\Debugbar;

use Faker\Factory;

use SnappyPDF;

use DateTime;

use File;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Url;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request\Validator;
use Illuminate\Support\Facades\Auth;


use Illuminate\Support\Facades\DB;

class APIController extends Controller
{

    public function imageupload(Request $request){ 

        $file =  $request->file("imageupload");

        $file_title = $file->getClientOriginalName();
       
        $file_ext = strtolower($file->getClientOriginalExtension());
        $rand_code  = "0";
        if($file_ext == "jpeg" || $file_ext == "jpg" || $file_ext == "png"){
        
            for($j=0; $j<9; $j++) {
                $min = ($j == 0) ? 1:0;
                $rand_code .= mt_rand($min,9);
            }

            $file->move('images/upload/', $rand_code.'.'.$file_ext);

            $url = route('index').'/'.'images/upload/'.$rand_code.'.'.$file_ext;
        
                
        }else{
            $url = "";
        }

        return response()->json(compact('url'));
    }

    public function createdfile(Request $request){
        $img = $request->get('file');
        $filename = $request->get('filename');

        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        file_put_contents('images/upload/'.$filename, $data);

        return route('index').'/'.'images/upload/'.$filename;
    }
}