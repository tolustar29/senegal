import Vue from 'vue'
import App from './views/App'
import router from './routes.js';


import axios from 'axios'
Vue.prototype.axios = axios;


import 'vue2-dropzone/dist/vue2Dropzone.css'


Vue.use(require('vue-script2'));

if (process.env.MIX_APP_ENV === 'production') {
  Vue.config.devtools = false;
  Vue.config.debug = false;
  Vue.config.silent = true;
}

const app = new Vue({
  el: '#app',
  router,
  components: {
    App
  },
});