import Vue from 'vue'
import VueRouter from 'vue-router'
import VueProgressBar from 'vue-progressbar'



import Home from './views/pages/Home'
import NotFound from "./views/pages/NotFound";
import Download from "./views/pages/Download";


import VueAnalytics from 'vue-ua'
 


Vue.use(VueRouter);

Vue.use(VueProgressBar, {
  color: '#99744c',
  failedColor: '#99744c',
  thickness: '9px',
  transition: {
    speed: '0.2s',
    opacity: '0.6s',
    termination: 300
  },
  autoFinish: true,
})


let current_url = window.location.hostname;
window.index_url = "";


if (current_url.includes("localhost")) {
  index_url = "/senegal";
}

let metaProgress = {
  func: [{
      call: 'color',
      modifier: 'temp',
      argument: '#99744c'
    },
    {
      call: 'fail',
      modifier: 'temp',
      argument: '#99744c'
    },
    {
      call: 'location',
      modifier: 'temp',
      argument: 'top'
    },
    {
      call: 'transition',
      modifier: 'temp',
      argument: {
        speed: '0.5s',
        opacity: '0.6s',
        termination: 400
      }
    }
  ]
}

let meta = {
  progress: metaProgress
};

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
    meta: meta
  },
  {
    path: "/download",
    name: "download",
    component: Download,
    meta: meta
  },
  {
    path: "/not-found",
    name: "notfound",
    component: NotFound,
    meta: meta
  },
];

const router = new VueRouter({
  scrollBehavior() {
    return {
      x: 0,
      y: 0
    };
  },
  routes,
  mode: 'history',
  base: index_url
});



let security = {
    guard(to, from, next){
        if (!to.matched.length) {
          next("/not-found");
        }
      
        next();

    }
}


router.beforeEach((to, from, next) => {
  
  security.guard(to, from, next);
  
})

Vue.use(VueAnalytics, {
  appName: 'Senegal Pour Tous', // Mandatory
  appVersion: '1', // Mandatory
  trackingId: 'UA-132571166-1', // Mandatory
  debug: true, // Whether or not display console logs debugs (optional)
  vueRouter: router, // Pass the router instance to automatically sync with router (optional)
  trackPage: true, // Whether you want page changes to be recorded as pageviews (website) or screenviews (app), default: false
})



export default router
